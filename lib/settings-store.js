"use strict"
/*jshint esversion:6*/
/*jslint node: true */
/** @module SettingsStore */

const Helpers = require("./helpers.js")
const os = require("os");
const fs = require("fs")
const path = require("path");

var overrides = {}
var store = {}
var defaults = {}

var initOpts = {}
var fileHandle
var fileWatcher

/*
Private functions
*/

const getDefaultFilename = function(opts) {
  let directory
  switch (process.platform) {
  case "win32":
    directory = process.env.APPDATA || path.join(os.homedir(), "AppData", "Roaming")
    break
  case "darwin":
    directory = path.join(os.homedir(), "Library" ,"Preferences")
    break
  default:
    directory = process.env.XDG_CONFIG_HOME || path.join(os.homedir(), ".config")
  }

  switch (process.platform) {
  case "darwin":
    if (typeof opts.reverseDNS === "undefined") {
      throw "opts.reverseDNS needs to be specified when initializing settings-store on macOS"
    }
    return path.join(directory, opts.reverseDNS + ".json")
  default:
    if (typeof opts.publisherName !== "undefined") {
      directory = path.join(directory, opts.publisherName)
    }
    if (typeof opts.appName === "undefined") {
      throw "opts.appName needs to be specified when initializing settings-store"
    }
    directory = path.join(directory, opts.appName)
    return path.join(directory, "settings.json")
  }
}

const updateFromFileSync = function() {
  let json = fs.readFileSync(initOpts.filename, "utf8")
  store = JSON.parse(json)
  Helpers.freezeObject(store)
}

const stopWatcher = function() {
  if (fileWatcher) {
    fileWatcher.close()
  }
}

const startWatcher = function() {
  if (initOpts.enableReloading === false) return
  if (!fs.existsSync(initOpts.filename)) return

  try {
    fileWatcher = fs.watch(initOpts.filename, {}, (eventType, filename) => {
        switch (eventType) {
        case "change":
          try {
            updateFromFileSync()
          } catch(e) {/*fail silently*/}
          break
        case "rename":
          stopWatcher()
          startWatcher()
        }
      })
  } catch(e) {
    console.log("Could not start watching settings file", initOpts.filename, e)
  }
}

const save = function() {
  stopWatcher()
  fs.writeFileSync(initOpts.filename, JSON.stringify(store))
  startWatcher()
}

const initElectronApp = function(app) {
  try {
    app = app ? app : require("electron").app
    let electronPath = app.getPath("userData")
    initOpts.filename = path.join(electronPath, "settings.json")
    initOpts.electronApp = app
  } catch(e) {
    initOpts.electronApp = false
  }
}

const SettingsStore = function() {
  initElectronApp()
  try {
    updateFromFileSync()
  } catch(e) {}
}

SettingsStore.prototype.init = function(opts) {
  //Detect Electron
  if (typeof opts.electronApp !== "undefined") {
    if (opts.electronApp === true) {
      initElectronApp()
    } else {
      initElectronApp(opts.electronApp)
    }
  }

  //Set custom filename
  if (typeof opts.filename !== "undefined") {
    initOpts.filename = path.normalize(opts.filename)
  } else if (typeof initOpts.filename === "undefined") {
    initOpts.filename = getDefaultFilename(opts)
  }

  //Make sure target folder exists
  Helpers.createDir(path.dirname(initOpts.filename))

  //Try loading settings
  try {
    updateFromFileSync()
  } catch(e) {
    try {
      let fd = fs.openSync(initOpts.filename, "w")
      fs.closeSync(fd)
    } catch(e) {
      throw e
    }
  }

  //Enable file watcher
  initOpts.enableReloading = typeof opts.enableReloading !== "undefined" ?
    opts.enableReloading :
    true
  startWatcher()
}

/** @function value
    @description Returns overridden, stored or default value for given key.
    Defaults passed as defaultValue to this function take precedence over
    those set with loadDefaults/setDefaults.
    All returned objects can be safely manipulated without altering the store
    @param {string|string[]} key [key] - can be a string with nested keys separated by "." or an array or nested key strings
    @param defaultValue - if no value has been set for the provided key, defaultValue will be returned. Takes precedence over previously set default values.
*/
SettingsStore.prototype.value = function(key, defaultValue) {
  let keys = Helpers.splitNestedKeys(key)

  let overriddenValue = Helpers.getIn(overrides, keys)
  if (typeof overriddenValue !== "undefined") return overriddenValue

  defaultValue = typeof defaultValue !== "undefined" ?
    defaultValue :
    Helpers.getIn(defaults, keys)
  let value = Helpers.getIn(store, keys)
  if (Helpers.isObject(value) && Helpers.isObject(defaultValue)) {
    return Helpers.setIn({result: defaultValue}, ["result"], value, true).result
  }
  return typeof value !== "undefined" ? value : defaultValue
}

/** @function setValue
    @description Changes the value of key to value and updates the settings file.
    @param {string|string[]} key - can be a string (also with nested keys, separated by ".") or an array of nested key strings
    @param {any} value - the new value for key. Can be any type. If `value` is an object, nested keys will also be stored.
    @param {bool} merge [merge=false] - Use when passing an object for *value*. Enables or disables merging of new *value* object with previously stored values.
*/
SettingsStore.prototype.setValue = function(key, value, merge) {
  //Make sure initOpts.filename has been initialized
  if (!initOpts.filename || typeof initOpts.filename !== "string") {
    throw "settings-store couldn’t determine filename. settings.init(opts) needs to be called first when not using Electron."
  }

  merge = typeof merge !== "undefined" ? merge : false
  let keys = Helpers.splitNestedKeys(key)
  let updatedStore = Helpers.setIn(store, keys, value, merge)
  Helpers.freezeObject(updatedStore)
  store = updatedStore
  save()
}

/** @function overrideValue
    @description Changes the value of *key* to *value* without updating the settings file.
    @param {string|string[]} key - can be a string (also with nested keys, separated by ".") or an array of nested key strings
    @param {any} value - the new value for *key*. Can be any type. If *value* is an object, nested keys will also be stored. If *value* is *undefined*, it will be treated as not overridden.
*/
SettingsStore.prototype.override = function(key, value) {
  let keys = Helpers.splitNestedKeys(key)
  overrides = Helpers.setIn(overrides, keys, value)
}

/** @function setDefault
    @description Set all default values at once.
    @param {object} defaultValues - object containing all desired default values
*/
SettingsStore.prototype.setDefaults = function(defaultValues) {
  defaults = Helpers.cloneObject(defaultValues)
  Helpers.freezeObject(defaults)
}

/** @function loadDefaults
    @description Reads default settings from a JSON file
    @param {string} filename - the complete path to a JSON file containing settings default values
*/
SettingsStore.prototype.loadDefaults = function(filename) {
  let json = fs.readFileSync(filename)
  defaults = JSON.parse(json)
  Helpers.freezeObject(defaults)
}

/** @function all
    @description Returns all currently set values. Does not include defaults or overrides.
*/
SettingsStore.prototype.all = function() {
  return store
}

/** @function clear
    @description Removes all entries. Does not reset defaults or overrides.
*/
SettingsStore.prototype.clear = function() {
  store = {}
  Helpers.freezeObject(store)
  save()
}

/** @function delete
    @description Removes an entry from the settings.
    @param {string|string[]} key - can be a string (also with nested keys, separated by ".") or an array of nested key strings
*/
SettingsStore.prototype.delete = function(key) {
  let keys = Helpers.splitNestedKeys(key)
  let updatedStore = Helpers.deleteIn(store, keys)
  Helpers.freezeObject(updatedStore)
  store = updatedStore
  save()
}

module.exports = new SettingsStore()
