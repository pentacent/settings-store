"use strict"
/*jshint esversion:6*/
/*jslint node: true */
/** @module SettingsStoreHelpers
 *  @private
*/

/**
 * Helper methods for settings-store
 */

const fs = require("fs")
const path = require("path")

/** Returns true if obj is an Object and not null */
const isObject = function(obj) {
  return typeof obj === "object" && obj !== null
}

/** Splits multiple keys joined with "." into an array */
const splitNestedKeys = function(key) {
  if (Array.isArray(key)) {
    return key
  }
  return key.split(".")
}

/** Deep-clones an object and returns a copy */
const cloneObject = function(obj) {
  if (!isObject(obj)) return obj
  if (Array.isArray(obj)) {
    return obj.map(value => cloneObject(value))
  }

  return Object.getOwnPropertyNames(obj)
    .reduce((acc, key) => {
      acc[key] = cloneObject(obj[key])
      return acc
    }, {})
}

/** Mutates input! Deep-frezes an object */
const freezeObject = function(obj) {
  if (!isObject(obj)) return

  let keys = Object.getOwnPropertyNames(obj)
  keys.forEach(key => {
    freezeObject(obj[key])
  })

  Object.freeze(obj)
}

/** Returns nested object value **/
const getIn = function(obj, keys) {
  return keys.reduce((acc, key) => {
    return typeof acc !== "undefined" ?
      acc[key] :
      undefined
  }, obj)
}

/** Mutates input! Sets object property to value, supports merging existing value objects. */
const set = function(obj, key, value, merge) {
  if (!isObject(value)) {
    obj[key] = value
    return obj
  } else if (Array.isArray(value)) {
    obj[key]= cloneObject(value)
    return obj
  }

  let keys = Object.getOwnPropertyNames(value)
  if (!merge) {
    obj[key] = {}
  }
  keys.forEach(valueKey => {
    if (!obj.hasOwnProperty(key)) {
      obj[key] = {}
    }
    set(obj[key], valueKey, value[valueKey])
  })
  return obj
}

/** Returns obj updated with given nested value */
const setIn = function(obj, keys, value, merge) {
  let updatedObj = cloneObject(obj)

  keys = cloneObject(keys)
  let lastKey = keys.pop()

  let leaf = updatedObj
  keys.forEach(key => {
    if (!leaf.hasOwnProperty(key)) {
      leaf[key] = {}
    } else if (!isObject(leaf[key])) {
      throw "settings-store: " + JSON.stringify(leaf) + "[" + key + "] is not an object."
    }
    leaf = leaf[key]
  })

  set(leaf, lastKey, value, merge)
  return updatedObj
}

/** Returns obj updated without given nested value */
const deleteIn = function(obj, keys) {
  let updatedObj = cloneObject(obj)
  keys = cloneObject(keys)
  let lastKey = keys.pop()
  let leaf = updatedObj
  keys.forEach(key => {
    if (!leaf || !leaf.hasOwnProperty(key)) {
      return //Don’t complain if there is nothing to delete
    }
    leaf = leaf[key]
  })
  if (!leaf) return updatedObj

  delete leaf[lastKey]
  return updatedObj
}

/** Recursively creates directory */
const createDir = function(directory) {
  let directoriesToCreate = []
  while (!fs.existsSync(directory)) {
    directoriesToCreate.unshift(directory)
    directory = path.dirname(directory)
  }
  directoriesToCreate.forEach(path => fs.mkdirSync(path))
}

module.exports = {
  cloneObject: cloneObject,
  freezeObject: freezeObject,
  getIn: getIn,
  splitNestedKeys: splitNestedKeys,
  setIn: setIn,
  deleteIn: deleteIn,
  createDir: createDir,
  isObject: isObject
}
