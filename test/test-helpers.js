"use strict"
/*jshint esversion:6*/
/*jslint node: true */

const {test} = require("ava")
const Helpers = require("../lib/helpers.js")

const fs = require("fs")
const os = require("os")
const path = require("path")

var testPath
test.before(() => {
  let tmpPath = fs.mkdtempSync(path.join(os.tmpdir(), "settings-store-helper-test"))
  testPath = path.join(tmpPath, "foo", "bar")
})

test.after(() => {
  let rmPath = testPath
  while (rmPath !== os.tmpdir()) {
    fs.rmdirSync(rmPath)
    rmPath = path.dirname(rmPath)
  }
})

test("split nested keys", t => {
  const keyPathString = "foo.bar.fizz.buzz"
  const keyPathArray = ["foo", "bar", "fizz", "buzz"]

  //Dot-notation gets split
  t.deepEqual(Helpers.splitNestedKeys(keyPathString), keyPathArray)

  //Arrays remain unchanged
  t.deepEqual(Helpers.splitNestedKeys(keyPathArray), keyPathArray)
})

test("clone objects", t => {
  const obj = {foo: {
    bar: 100,
    buzz: "buzz",
    fizz: false,
    foo2: {bar2: 1000, buzz2: "buzz2"}
  }}
  let clonedObj = Helpers.cloneObject(obj)

  //Values of cloned object are identical
  t.deepEqual(clonedObj, obj)

  //Object has in fact been cloned and not referenced
  t.not(clonedObj, obj)
  t.not(clonedObj.foo, obj.foo)
  t.not(clonedObj.foo.foo2, obj.foo.foo2)
})

test("freeze object", t => {
  const objValues = {foo: {
    bar: 100,
    buzz: "buzz",
    fizz: false,
    foo2: {bar2: 1000, buzz2: "buzz2"}
  }}
  let obj = Helpers.cloneObject(objValues)

  //Nothing is returned
  t.is(Helpers.freezeObject(obj), undefined)

  //Values of frozen object remains unchanged
  t.deepEqual(objValues, obj)

  //Object has in fact been frozen
  t.true(Object.isFrozen(obj))
  t.true(Object.isFrozen(obj.foo))
  t.true(Object.isFrozen(obj.foo.foo2))
})

test("getting nested values", t => {
  const obj = {foo: {
    bar: 100,
    buzz: "buzz",
    fizz: false,
    foo2: {bar2: 1000, buzz2: "buzz2"}
  }}

  //Fails when not using keypath array
  t.throws(() => Helpers.getIn(obj, "foo.bar"))

  //Retrieving values with keypath array
  t.is(Helpers.getIn(obj, ["foo", "bar"]), obj.foo.bar)
  t.is(Helpers.getIn(obj, ["foo", "foo2", "buzz2"]), obj.foo.foo2.buzz2)

  //Non-existant keypaths return undefined values
  t.is(Helpers.getIn(obj, ["fooz", "fizz", "buzz"]), undefined)
})

test("setting and merging values", t => {
  const objValues = {a: {b: 1}}
  const mergeData = {c: 2, d: {e: 1, f: 2}}

  //Setting value without merging
  let mergedObj = Helpers.setIn(objValues, ["a"], mergeData)
  t.deepEqual(mergedObj.a, mergeData)

  //Setting value and merging
  mergedObj = Helpers.setIn(objValues, ["a"], mergeData, true)
  t.deepEqual(mergedObj.a, {b: 1, c:2, d: {e: 1, f: 2}})

  //Non-existant key paths get created
  mergedObj = Helpers.setIn({}, ["a", "b"], {c: {d: true}}, true)
  t.deepEqual(mergedObj, {a: {b: {c: {d: true}}}})

  //Having non-objects as non-leaves in the key path doesn’t work
  t.throws(() => Helpers.setIn(objValues, ["a", "b", "c"], true))

  //Usings arrays as values
  const arr = [1,2,3]
  mergedObj = Helpers.setIn(objValues, ["arr"], arr)
  //Arrays get copied
  t.not(mergedObj.arr, arr)
  //But are still identical content-wise
  t.deepEqual(mergedObj.arr, arr)
})

test("deleting nested values", t => {
   const objValues = {a: {b: {c: 1}}, d: 2}
   const deletedA = {d: 2}
   const deletedB = {a: {}, d: 2}
   const deletedC = {a: {b: {}}, d: 2}
   const deletedD = {a: {b: {c: 1}}}

   t.deepEqual(objValues, Helpers.deleteIn(objValues, ["x"]))
   t.deepEqual(deletedA, Helpers.deleteIn(objValues, ["a"]))
   t.deepEqual(deletedB, Helpers.deleteIn(objValues, ["a", "b"]))
   t.deepEqual(deletedC, Helpers.deleteIn(objValues, ["a", "b", "c"]))
   t.deepEqual(deletedD, Helpers.deleteIn(objValues, ["d"]))
})

test("recursively creating directories", t => {
  Helpers.createDir(testPath)
  t.is(fs.existsSync(testPath), true)
})
