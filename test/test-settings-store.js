"use strict"
/*jshint esversion:6*/
/*jslint node: true */

const {test} = require("ava")
const settings = require("../lib/settings-store")

const fs = require("fs")
const os = require("os")
const path = require("path")

var testPath
test.before(() => {
  let tmpPath = fs.mkdtempSync(path.join(os.tmpdir(), "settings-store-test"))
  testPath = path.join(tmpPath, "config", "settings.json")
})

test.after.always(() => {
  if (fs.existsSync(testPath)) fs.unlinkSync(testPath)
  let rmPath = path.dirname(testPath)
  while (rmPath !== os.tmpdir()) {
    fs.rmdirSync(rmPath)
    rmPath = path.dirname(rmPath)
  }
})
test.serial("exception when using uninitialized", t => {
  t.throws(() => settings.setValue("foo", "bar"))
})

test.serial("initializing", t => {
  t.throws(() => settings.init())
  t.notThrows(() => settings.init({
    appName: "foo",
    publisherName: "bar",
    filename: testPath
  }))
})

test.serial("retrieving undefined keys returns undefined", t => {
  t.is(settings.value(["foo", "bar"], undefined))
})

test.serial("saving null values", t => {
  settings.clear()

  settings.setValue("foo", null)
  t.is(settings.value("foo"), null)
  settings.setValue("fizz.buzz", null)
  t.is(settings.value("fizz.buzz"), null)
})

test.serial("correct precedences for defaults and overrides", t => {
  settings.clear()
  settings.setDefaults({
    foo: {bar: 1}
  })

  //Global defaults
  t.is(settings.value(["foo", "bar"]), 1)

  //Explicit defaults
  t.is(settings.value(["foo", "bar"], 2), 2)

  //Set value
  settings.setValue(["foo", "bar"], 3)
  t.is(settings.value(["foo", "bar"], 2), 3)

  //Overrides
  settings.override(["foo", "bar"], 4)
  t.is(settings.value(["foo", "bar"], 2), 4)
})

test.serial("merged defaults", t => {
  settings.clear()
  settings.setDefaults({
    fizzbuzz: {
      fizz: 1,
      buzz: 1
    }
  })
  settings.setValue(["fizzbuzz", "buzz"], 2)
  t.deepEqual(settings.value("fizzbuzz"), {fizz: 1, buzz: 2})
})

test.serial("retrieving and clearing all values", t => {
  settings.clear()
  settings.setValue("foo", "bar")
  settings.setValue("fizz", "buzz")
  t.deepEqual(settings.all(), {foo: "bar", fizz: "buzz"})
  settings.clear()
  t.deepEqual(settings.all(), {})
})

test.serial("deleting values", t => {
  settings.clear()
  settings.setValue("foo", "bar")
  settings.setValue("fizz.buzz", "foobar")
  settings.delete("fizz.buzz")
  t.deepEqual(settings.all(), {foo: "bar", fizz: {}})
  settings.delete("fizz")
  t.deepEqual(settings.all(), {foo: "bar"})
})

