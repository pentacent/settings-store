# settings-store

> Cross-platform settings and configuration store for Node.js. A great choice for CLI apps that need to persist user data. Also has dedicated support for Electron.

## Why should I use settings-store?
Here are just some of the awesome features *settings-store* has to offer:

- Simple but powerful API
- Support for apps with and without Electron
- Works on Windows, macOS, Linux & *ix
- Live-reloading of changes to the settings file
- Setting default values in bulk or for individual keys
- Temporarily overriding settings without saving them
- No additional dependencies

## Installing
You can install *settings-store* via npm.
Add *settings-store* to your `package.json` or install it via the command-line:
`npm i --save settings-store`

## Basic usage
Use *settings-store* in your application like this:

```javascript
const settings = require("settings-store")

//Initializing is optional when using Electron
settings.init({
    appName:       "Foo", //required,
    publisherName: "Bar", //optional
    reverseDNS:    "com.bar.foo" //required for macOS
})

let foo = settings.value("foo", 0)
// foo hasn’t been set before and will be: 0

settings.setValue("foo", 1)
let updatedFoo = settings.value("foo", 0)
// updatedFoo will be: 1
```

You can also use nested key paths by either using dot notation or passing them as an array:
```javascript
let name = {first: "Jane", last: "Doe"}
settings.setValue("name", name)

let firstName = settings.value("name.first")
// firstName will be: "Jane"

let lastName = settings.value(["name", "last"])
// lastName will be: "Doe"

settings.setValue("name.first", "John")
let updatedName = settings.value("name")
// name will be: {first: "John", last: "Doe"}
```

## Deleting properties
You can delete any property with `settings.delete(key)`:
```javascript
let foo = {bar: "foo", foo: "bar"}
settings.setValue("foo", foo)

settings.delete("foo.bar")
let updatedFoo = settings.value("foo")
// updatedFoo will be: {foo: "bar"}
```
If the given key or key path doesn’t exist, `settings.delete(key)` will not
throw an error.

## Merging nested properties
When working with nested properties, you can choose whether to merge them with previous values or not by using `settings.setValue(key, value, merge=false)`:
```javascript
let foo = {bar: "foo", foo: "bar"}
settings.setValue("foo", foo)
let buzz = {buzz: "fizz", fizz: "buzz"}

//With merging
settings.setValue("foo", buzz, true)
let merged = settings.value("foo")
// merged will be: {bar: "foo", foo: "bar", buzz: "fizz", fizz: "buzz"}

//Without merging
settings.setValue("foo", buzz, false)
let unmerged = settings.value("foo")
// unmerged will be: {buzz: "fizz", fizz: "buzz"}
```

## Setting default values
You can use `settings.value(key, defaultValue)` when retrieving a value if you want to use a default value in case no other value has been set for `key` yet.

Alternatively, you can also use `settings.loadDefaults(filename)` or `settings.setDefaults(settings)`:
```javascript
settings.setDefaults({
    name: {
        first: "Jane",
        last: "Doe"
    }
})

let firstName = settings.value("name.first")
// name.first hasn’t been set yet and will default to: "Jane"
```

Default values will not be stored to your settings file and existing values in the settings file will take precedence over previously set defaults.

## Using overrides
If you want to temporarily override some of your user’s settings, you can use `settings.override(key, value)`. Overridden values will take precedence over both defaults and values set with `settings.setValue(key, value)` but won’t be stored to the settings file.

```javascript
settings.setValue("foo", "bar")
settings.override("foo", "baz")
let foo = settings.value("foo")
// foo will be: "baz"
```

This can be especially handy if you want to override settings with values from environment variables.

## Retrieving/clearing all settings
If you want to retrieve all settings, you can use `settings.all()`. This will give you the complete settings store, ignoring overrides and defaults.
Clearing all settings can be achieved with `settings.clear()`. This will delete all properties stored with `settings.setValue(key, value)`. Overrides and defaults will not be reset.

## Configuring settings-store
*settings-store* needs very little configuration. Simply call `settings.init(opts)` with your desired options before otherwise using the module.

### Using Electron
If you are using Electron, your settings file will be stored in the Electron `userData` path (see [Electron docs](https://electron.atom.io/docs/api/app/#appgetpathname) for more information). **In this case, no configuration is required at all**.

### Without Electron
For applications that do not use Electron, you need to provide your application name and the [reverse-DNS string](https://en.wikipedia.org/wiki/Reverse_domain_name_notation) and *settings-store* will create a settings file in a folder adequate for the user’s operating system:
```javascript
/*Required configuration when not using Electron*/
const settings = require("settings-store")
const settingsOpts = {
    appName:       "Foo", //required,
    publisherName: "Bar", //optional
    reverseDNS:    "com.bar.foo" //required for macOS
}
settings.init(settingsOpts)
```

### Optional options
- `electronApp` Object | Boolean, default `true`
    Use this to explicitly pass your `electron.app` object. If you want to explicitly enable or disable automatic Electron detection, you can set `electronApp` to `true` or `false`.
- `enableReloading` Boolean, default: `true`
Enable or disable reloading the settings file if it has been changed outside of the application
- `filename` String
    Use this to override the automatically generated path of your settings file

## Storable types
You can store the following types with *settings-store*:

- `Array`
- `boolean`
- `null`
- `number`
- `string`

`Objects` will be considered nested properties that can have the same types for values as outlined above.

Storing non-plain objects (e. g. `Dates` or custom classes) is not supported.

Keys with `undefined` values will be considered not set.

## Similar modules
There are quite a few other modules offering similar functionality out there:

- [electron-settings](https://www.npmjs.com/package/electron-settings)
  Only for Electron apps, no overrides, when settings defaults, they are stored permanently in the settings file
- [electron-json-storage](https://www.npmjs.com/package/electron-json-storage)
  Only for Electron apps, focuses on storing and retrieving full JSON documents
- [configstore](https://www.npmjs.com/package/configstore)
  No support for macOS/Windows, no overrides
- [conf](https://www.npmjs.com/package/conf)
  No overrides, only global defaults

## Contributing
Contributions and bug reports are welcome. You can create pull requests and issues in the [*settings-store* Bitbucket repository](https://bitbucket.org/pentacent/settings-store).

## Credits
*settings-store* is being developed by [Pentacent](https://pentacent.com) for [DBLSQD](https://dblsqd.coḿ) and [now.do](https://now.do).

The API of *settings-store* has been partially inspired by [QSettings](https://doc.qt.io/qt-5/qsettings.html), the settings class of the Qt C++ framework.

## License
*settings-store* is made available under the terms of the MIT license. Please read the LICENSE file carefully before using *settings-store* in one of your projects.
